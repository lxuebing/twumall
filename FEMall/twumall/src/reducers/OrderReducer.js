const initState = {
	order: {},
	orderList:[]
};

export default (state = initState, action) => {
	switch (action.type) {
		case 'CREATE_ORDER':
			return {
				...state
			};
		case 'GET_ORDER':
			return {
				...state,
				orderList: action.orderList
			};
		case 'DELETE_ORDER':
			return {
				...state,
				orderList: action.orderList
			};
		default:
			return state
	}
};
