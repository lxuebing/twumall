const initState = {
	product: {},
	productList: []
};

export default (state = initState, action) => {
	switch (action.type) {
		case 'ADD_PRODUCT':
			return {
				...state
			};
		case 'GET_PRODUCT':
			return {
				...state,
				productList: action.productList
			};
		default:
			return state
	}
};
