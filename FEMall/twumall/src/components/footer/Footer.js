import React, {Component} from 'react';
import "./Footer.less";

class Footer extends Component {
	render() {
		return (
			<div className={"footer-wrap"}>
				<span>TW Mall @2018 Created by ForCheng</span>
			</div>
		);
	}
}

export default Footer;