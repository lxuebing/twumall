import React, {Component} from 'react';
import './OrderItem.less'

class OrderItem extends Component {
	render() {
		return (
			<div className={"order-table-th"}>
				<span>{this.props.item.product.name}</span>
				<span>{this.props.item.product.price}</span>
				<span>{this.props.item.count}</span>
				<span>{this.props.item.product.unit}</span>
				<span>
					<button onClick={this.props.delete}>删除</button>
				</span>
			</div>
		);
	}
}

export default OrderItem;