import React, {Component} from 'react';
import "./Product.less";
import { IconContext } from "react-icons";
import { MdAddCircleOutline } from 'react-icons/md';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {createOrder} from "../../actions/createOrderAction";

class Product extends Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			product: this.props.product
		}
		this.handleAddProduct = this.handleAddProduct.bind(this);
	}

	handleAddProduct(){
		this.props.createOrder(this.props.product.id);
	}

	render() {
		let product = this.props.product;
		return (
			<IconContext.Provider value={{ style: {color: "#000", verticalAlign: 'middle'}}}>
				<div className={"product-wrap"}>
					<img src={product.imgUrl} alt="商品图片"/>
					<p>{product.name}</p>
					<p>单价：{product.price}元/{product.unit}</p>
					<button className="Product-icon" onClick={this.handleAddProduct}>
						<MdAddCircleOutline size={24}/>
					</button>
				</div>
			</IconContext.Provider>
		);
	}
}

const mapStateToProps = state => ({
	order: state.orderAll.order,
});

const mapDispatchToProps = dispatch => bindActionCreators({
	createOrder
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Product);

