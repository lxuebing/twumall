export const createOrder = (product_id) => (dispatch) => {
	fetch("http://localhost:8181/api/order", {
		method: 'POST',
		headers:{
			'Accept': 'application/json',
			'content-type':'application/json'
		},
		body: JSON.stringify({
			product_id:product_id
		})
	}).then((result) => {
			dispatch({
				type: 'CREATE_ORDER'
			});

		}).catch(() => {
		console.log("fetch error");
	})
};