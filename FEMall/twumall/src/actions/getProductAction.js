export const getProducts = () => (dispatch) => {
	fetch("http://localhost:8181/api/products", {
		method: 'GET',
		headers:{
			'Accept': 'application/json',
			'content-type':'application/json'
		}
	}).then(result => result.json())
.then((result) => {
			dispatch({
				type: 'GET_PRODUCT',
				productList: result
			});
		}).catch(() => {
		console.log("fetch error");
	})
};