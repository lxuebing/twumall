export const getOrders = () => (dispatch) => {
	fetch("http://localhost:8181/api/orders", {
		method: 'GET',
		headers:{
			'Accept': 'application/json',
			'content-type':'application/json'
		}
	}).then(result => result.json())
		.then((result) => {
			dispatch({
				type: 'GET_ORDER',
				orderList: result
			});
		}).catch(() => {
		console.log("fetch error");
	})
};