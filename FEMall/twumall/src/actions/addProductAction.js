export const addProduct = (name,unit,imgUrl,price,props) => (dispatch) => {
	fetch("http://localhost:8181/api/product", {
		method: 'POST',
		headers:{
			'Accept': 'application/json',
			'content-type':'application/json'
		},
		body: JSON.stringify({
			'name':name,
			'unit':unit,
			'imgUrl':imgUrl,
			'price':price
		})
	}).then(() =>{
			dispatch({
				type: 'ADD_PRODUCT'
			});
			props.history.push("/");
		}).catch(() => {
		console.log("fetch error");
	})
};