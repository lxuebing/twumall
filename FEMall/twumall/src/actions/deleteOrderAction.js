export const deleteOrder = (product_id) => (dispatch) => {
	fetch("http://localhost:8181/api/order/" + product_id, {
		method: 'DELETE',
		headers: {
			'Accept': 'application/json',
			'content-type': 'application/json'
		}
	}).then(res => res.json())
		.then((result) => {
			dispatch({
				type: 'DELETE_ORDER',
				orderList: result
			});

		}).catch(() => {
		console.log("fetch error");
	})
};