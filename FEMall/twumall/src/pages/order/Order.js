import React, {Component} from 'react';
import "./Order.less"
import OrderItem from "../../components/orderItem/OrderItem";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {getOrders} from "../../actions/getOrderAction";
import {deleteOrder} from "../../actions/deleteOrderAction";

class Order extends Component {

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {
		this.props.getOrders();
	}

	render() {
		return (
			<div className={"order-wrap"}>
				<div className={"order-table-th"}>
					<div>名字</div>
					<div>单价</div>
					<div>数量</div>
					<div>单位</div>
					<div>操作</div>
				</div>
				{this.props.orderList.map((item, key) =>
					<OrderItem item={item} key={key} delete={this.props.deleteOrder.bind(this, item.id)}/>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	orderList: state.orderAll.orderList,
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getOrders,
	deleteOrder
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);