import React, {Component} from 'react';
import Product from "../../components/product/Product";
import connect from "react-redux/es/connect/connect";
import {getProducts} from "../../actions/getProductAction";
import {bindActionCreators} from "redux";
import "./Home.less";

class Home extends Component {

	componentDidMount() {
		this.props.getProducts();
	}

	render() {
		return (
			<div className={'home'}>
				{
					this.props.productList.map((product,key) =>
						<Product key={key} product={product} className={'product-item'}/>
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	productList: state.productAll.productList,
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);