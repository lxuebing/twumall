import React, {Component} from 'react';
import "./AddProduct.less"
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {addProduct} from "../../actions/addProductAction";

class AddProduct extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			name:'',
			unit:'',
			imgUrl:'',
			price:'',
		}

		this.handleName = this.handleName.bind(this);
		this.handlePrice = this.handlePrice.bind(this);
		this.handleUnit = this.handleUnit.bind(this);
		this.handleImg = this.handleImg.bind(this);
		this.handleAddProduct = this.handleAddProduct.bind(this);
	}

	handleName(ev){
		this.state.name = ev.target.value
	}

	handlePrice(ev){
		this.state.price = ev.target.value
	}

	handleUnit(ev){
		this.state.unit = ev.target.value
	}

	handleImg(ev){
		this.state.imgUrl = ev.target.value
	}

	handleAddProduct(){
		this.props.addProduct(this.state.name,this.state.unit,this.state.imgUrl,this.state.price,this.props);
	}

	render() {
		return (
			<div className={"add-product-wrap"}>
				<div className={"add-product"}>
					<p>添加商品</p>
					<div className={"label-input"}>
						<label htmlFor="name"><span>＊</span>名称:</label>
						<br/>
						<input type="text" name="name" id="name" placeholder={"商品名称"} onChange={this.handleName}/>
					</div>
					<div className={"label-input"}>
						<label htmlFor="price"><span>＊</span>价格:</label>
						<br/>
						<input type="text" name="price" id="price" placeholder={"商品价格"} onChange={this.handlePrice}/>
					</div>
					<div className={"label-input"}>
						<label htmlFor="unit"><span>＊</span>单位:</label>
						<br/>
						<input type="text" name="unit" id="unit" placeholder={"单位"} onChange={this.handleUnit}/>
					</div>
					<div className={"label-input"}>
						<label htmlFor="img"><span>＊</span>图片:</label>
						<br/>
						<input type="text" name="img" id="img" placeholder={"图片地址"} onChange={this.handleImg}/>
					</div>
					<button onClick={this.handleAddProduct}>submit</button>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	product: state.productAll.product,
});

const mapDispatchToProps = dispatch => bindActionCreators({
	addProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);