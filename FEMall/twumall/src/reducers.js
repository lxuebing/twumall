import { combineReducers } from 'redux';
import ProductReducer from "./reducers/ProductReducer";
import OrderReducer from "./reducers/OrderReducer";

export default combineReducers({
	productAll: ProductReducer,
	orderAll: OrderReducer
});