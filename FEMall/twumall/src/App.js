import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router, NavLink} from "react-router-dom";
import {Route} from "react-router";
import Home from "./pages/home/Home";
import AddProduct from "./pages/addProdeuct/AddProduct";
import Order from "./pages/order/Order";
import Footer from "./components/footer/Footer";
import { IconContext } from "react-icons";
import { IoIosHome, IoIosAdd, IoIosCart } from 'react-icons/io';

class App extends Component{
  render() {
    return (
      <IconContext.Provider value={{ style: {color: "#fff", verticalAlign: 'middle'}}}>
        <div className="App">
          <Router>
            <div className={"app-nav"}>
              <NavLink exact to='/' activeClassName='app-h' className='app-a'><IoIosHome size={20} className={"icon"}/>商城</NavLink>
              <NavLink to='/mall/order' activeClassName='app-h' className='app-a'><IoIosCart size={20} className={"icon"}/>订单</NavLink>
              <NavLink to='/mall/add' activeClassName='app-h' className='app-a'><IoIosAdd size={20} className={"icon"}/>添加商品</NavLink>
            </div>

            <Route exact path="/" component={Home}/>
            <Route path="/mall/order" component={Order}/>
            <Route path="/mall/add" component={AddProduct}/>
          </Router>

          <Footer />
        </div>
      </IconContext.Provider>
    );
  }
}

export default App;