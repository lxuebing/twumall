package com.twu.twumall.controller;

import com.twu.twumall.contract.AddOrderRequest;
import com.twu.twumall.domain.Order;
import com.twu.twumall.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RequestMapping("/api")
@RestController
@CrossOrigin(origins = "*")
public class OrderController {
    OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/order")
    public ResponseEntity createOrder(@RequestBody @Valid AddOrderRequest addOrderRequest, HttpServletRequest request){
        return orderService.createOrUpdateOrder(addOrderRequest,request);
    }

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getOrder(){
        List<Order> orders = orderService.getOrder();
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(orders);
    }

    @DeleteMapping("/order/{id}")
    public ResponseEntity<List<Order>> deleteOrder(@PathVariable Long id){
        List<Order> orders = orderService.deleteOrder(id);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(orders);
    }

}
