package com.twu.twumall.contract;

import com.twu.twumall.domain.Order;
import com.twu.twumall.domain.Product;

import javax.validation.constraints.NotNull;

public class AddOrderRequest {
    @NotNull
    Long product_id;

    public AddOrderRequest(Long product_id) {
        this.product_id = product_id;
    }

    public AddOrderRequest() {
    }

    public Long getProduct_id() {
        return product_id;
    }

    public Order transToOrder(Product product){
        return new Order(product, 1);
    }
}
