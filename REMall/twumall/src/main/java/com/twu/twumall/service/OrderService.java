package com.twu.twumall.service;

import com.twu.twumall.contract.AddOrderRequest;
import com.twu.twumall.domain.Order;
import com.twu.twumall.domain.Product;
import com.twu.twumall.exception.ResourceNotFoundExpection;
import com.twu.twumall.repository.OrderRepository;
import com.twu.twumall.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    OrderRepository orderRepository;
    ProductRepository productRepository;

    public OrderService(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    public ResponseEntity createOrUpdateOrder(AddOrderRequest addOrderRequest, HttpServletRequest request) {
        Optional<Order> byProduct_id = orderRepository.findByProduct_Id(addOrderRequest.getProduct_id());
        Order order;
        if (byProduct_id.isPresent()) {
            order = byProduct_id.get();
            order.setCount(order.getCount() + 1);

        } else {
            Product product = productRepository.findById(addOrderRequest.getProduct_id()).orElseThrow(ResourceNotFoundExpection::new);
            order = addOrderRequest.transToOrder(product);
        }
        orderRepository.save(order);
        return ResponseEntity.status(201)
                .header("location", request.getRequestURL() + "/" + order.getId())
                .build();
    }

    public List<Order> getOrder() {
        List<Order> all = orderRepository.findAll();
        return all;
    }

    public List<Order> deleteOrder(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(ResourceNotFoundExpection::new);
        orderRepository.delete(order);
        List<Order> all = orderRepository.findAll();
        return all;
    }


}
