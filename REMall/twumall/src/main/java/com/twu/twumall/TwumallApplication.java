package com.twu.twumall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwumallApplication {
    public static void main(String[] args) {
        SpringApplication.run(TwumallApplication.class, args);
    }
}
