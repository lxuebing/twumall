package com.twu.twumall.controller;

import com.twu.twumall.contract.AddProductRequest;
import com.twu.twumall.domain.Product;
import com.twu.twumall.service.ProductService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.List;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProductController {
    final
    ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/product")
    public ResponseEntity<Product> createStudent(@RequestBody @Valid AddProductRequest addProductRequest,HttpServletRequest request){
        Product product = new Product(addProductRequest);
        Long id = productService.postProduct(product).getId();
        productService.postProduct(product);
        return ResponseEntity.status(201)
                .contentType(MediaType.APPLICATION_JSON)
                .header("location", request.getRequestURL() + "/" + id)
                .header("id", id + "")
                .build();
    }

    @GetMapping("products")
    public ResponseEntity<List<Product>> getProduct(){
        List<Product> products = productService.getProduct();
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(products);
    }
}
