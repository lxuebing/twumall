package com.twu.twumall.contract;

import com.twu.twumall.domain.Order;
import com.twu.twumall.domain.Product;

import javax.validation.constraints.NotNull;

public class AddProductRequest {
    @NotNull
    String name;
    @NotNull
    String unit;
    @NotNull
    String imgUrl;
    @NotNull
    Long price;

    public AddProductRequest(@NotNull String name, @NotNull String unit, @NotNull String imgUrl, @NotNull Long price) {
        this.name = name;
        this.unit = unit;
        this.imgUrl = imgUrl;
        this.price = price;
    }

    public AddProductRequest() {
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Long getPrice() {
        return price;
    }

}
