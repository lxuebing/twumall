package com.twu.twumall.repository;

import com.twu.twumall.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order,Long> {
    Optional<Order> findByProduct_Id(Long id);
}
