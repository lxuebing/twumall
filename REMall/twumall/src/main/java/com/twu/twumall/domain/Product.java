package com.twu.twumall.domain;

import com.twu.twumall.contract.AddProductRequest;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false, length = 64)
    String name;

    @Column(nullable = false,length = 10)
    String unit;

    @Column(nullable = false)
    String imgUrl;

    @Column(nullable = false)
    Long price;



    public Product(String name, String unit, String imgUrl, Long price) {
        this.name = name;
        this.unit = unit;
        this.imgUrl = imgUrl;
        this.price = price;
    }

    public Product() {
    }

    public Product(AddProductRequest request) {
        this(request.getName(), request.getUnit(), request.getImgUrl(), request.getPrice());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Long getPrice() {
        return price;
    }
}
