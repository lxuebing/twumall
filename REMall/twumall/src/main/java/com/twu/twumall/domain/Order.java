package com.twu.twumall.domain;

import javax.persistence.*;

@Entity(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(nullable = false)
    int count;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    Product product;

    public Order(Product product, int count) {
        this.count = count;
        this.product = product;
    }

    public Order(Product product) {
        this.product = product;
    }

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public Product getProduct() {
        return product;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
