package com.twu.twumall.service;

import com.twu.twumall.domain.Product;
import com.twu.twumall.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    final
    ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product postProduct(Product product){
        return productRepository.save(product);
    }

    public List<Product> getProduct(){
        return productRepository.findAll();
    }
}
