CREATE TABLE IF NOT EXISTS orders (
    id BIGINT AUTO_INCREMENT,
    count INT(11) NOT NULL,
    product_id BIGINT NOT NULL,
    PRIMARY KEY (id)
);

alter table orders
add constraint fk_product
foreign key (product_id) references product(id);