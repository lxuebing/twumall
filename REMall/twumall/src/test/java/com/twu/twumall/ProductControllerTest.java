package com.twu.twumall;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twu.twumall.contract.AddProductRequest;
import com.twu.twumall.domain.Product;
import com.twu.twumall.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest extends IntegrationTestBase{

    @Autowired
    ProductRepository productRepository;

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void should_return_201_when_post_product() throws Exception {
        AddProductRequest addProductRequest = new AddProductRequest("可乐","瓶","https://timgsa.baidu.com/timg?image&amp;quality=80&amp;size=b9999_10000&amp;sec=1565080149924&amp;di=867a81012a874e6706b758a8ae8d5182&amp;imgtype=0&amp;src=http%3A%2F%2Fimg.39yst.com%2Fuploads%2F2015%2F0610%2F1433931232163.jpg",5L);
        String productString = objectMapper.writeValueAsString(addProductRequest);
        getMockMvc().perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(productString))
                .andExpect(status().is(201))
                .andExpect(header().string("location", "http://localhost/api/product/1"));
    }

    @Test
    void should_return_200_when_get_product() throws Exception {
        getMockMvc().perform(get("/api/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
}
