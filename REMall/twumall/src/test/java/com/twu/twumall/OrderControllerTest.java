package com.twu.twumall;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twu.twumall.contract.AddOrderRequest;
import com.twu.twumall.contract.AddProductRequest;
import com.twu.twumall.domain.Order;
import com.twu.twumall.domain.Product;
import com.twu.twumall.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Objects;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OrderControllerTest extends IntegrationTestBase{
    @Autowired
    OrderRepository orderRepository;

    private MvcResult mvcResult;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() throws Exception {
        saveProduct();
        saveOrder();
    }

    public void saveProduct() throws Exception {
        AddProductRequest addProductRequest = new AddProductRequest("xue","s","a",2L);
        String productString = objectMapper.writeValueAsString(addProductRequest);

        getMockMvc().perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(productString));
    }

    public void saveOrder() throws Exception {
        AddOrderRequest addOrderRequest = new AddOrderRequest(1L);
        String valueAsString = objectMapper.writeValueAsString(addOrderRequest);

        mvcResult = getMockMvc().perform(post("/api/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(valueAsString)).andReturn();
    }

    @Test
    void should_return_201_when_post_order() throws Exception {
        assertEquals(201, mvcResult.getResponse().getStatus());
    }

    @Test
    void should_return_200_when_get_order() throws Exception {
        getMockMvc().perform(get("/api/orders")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200)).andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    void should_return_200_when_delete_order() throws Exception {
        getMockMvc().perform(delete("/api/order/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
}
